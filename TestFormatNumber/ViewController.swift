//
//  ViewController.swift
//  TestFormatNumber
//
//  Created by Nheng Vanchhay on 25/6/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var formatPh     : UITextField!
    @IBOutlet weak var txtPh2       : UITextField!
    @IBOutlet weak var txtCurrency  : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.formatPh.addTarget(self, action: #selector(formatPh(_ :)), for: .editingChanged)
        self.txtPh2.addTarget(self, action: #selector(formatPh2(_ :)), for: .editingChanged)
        self.txtCurrency.addTarget(self, action: #selector(currency(_ :)), for: .editingChanged)
    }

    //pattern ###-##-#####
    @objc func formatPh(_ sender: UITextField) {
        
        if sender.text?.count ?? 0 > 13 {
            let txt =  "\((sender.text)?.dropLast() ?? "")"
            self.formatPh.text = txt.applyPatternOnNumbers(pattern: "###-##-#####", replacementCharacter: "#")
        }else {
            self.formatPh.text = sender.text?.applyPatternOnNumbers(pattern: "###-##-#####", replacementCharacter: "#")
        }
    }
    
    //pattern ##-####-####
    @objc func formatPh2(_ sender: UITextField) {
        
        if sender.text?.count ?? 0 > 12 {
            let txt =  "\((sender.text)?.dropLast() ?? "")"
            self.txtPh2.text = txt.applyPatternOnNumbers(pattern: "##-####-####", replacementCharacter: "#")
        }else{
            self.txtPh2.text = sender.text?.applyPatternOnNumbers(pattern: "##-####-####", replacementCharacter: "#")
        }
        
    }
    
    //currey 1,000 || 11,000,000
    @objc func currency(_ sender: UITextField) {
        if sender.text == "" || sender.text == "." || sender.text == "," {
            txtCurrency.text = ""
            return
        }else{
            let lastStr = "" + ((sender.text)?.suffix(1))! // convert to string
            
            if (sender.text)!.contains(".") {
                
                let strDevide = sender.text?.components(separatedBy: ".")
                if let valueBeforeDot = Double(strDevide?[0].replacingOccurrences(of: ",", with: "") ?? "") {
                    
                    txtCurrency.text = txtCurrency.text?.formattingNumber(valueBeforeDot) ?? "" + "." + ((strDevide?[1].prefix(2))?.replacingOccurrences(of: ",", with: ""))!
                }
            }else if lastStr.contains(",") { // version upper 14.4
                txtCurrency.text = ((sender.text)?.dropLast())! + "."
            }
            else {
                if let value = Double(sender.text?.replacingOccurrences(of: ",", with: "") ?? "") {
                    txtCurrency.text = txtCurrency.text?.formattingNumber(value)
                }
            }
            
        }
    }
}

extension String {
    func applyPatternOnNumbers(pattern: String, replacementCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(utf16Offset: index, in: pattern)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacementCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    func formattingNumber(_ num: Double) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: num as NSNumber)!
    }
}
